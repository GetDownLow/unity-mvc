﻿/// <summary>
/// Extension of the BaseApplication class to handle different types of Model View Controllers.
/// </summary>
/// <typeparam name="M"></typeparam>
/// <typeparam name="V"></typeparam>
/// <typeparam name="C"></typeparam>
public class BaseApplication<M, V, C> : BaseApplication
	where M : Element
	where V : Element
	where C : Element
{
	/// <summary>
	/// Model reference using the new type.
	/// </summary>
	public new M Model
	{
		get { return (M) (object) base.Model; }
	}

	/// <summary>
	/// View reference using the new type.
	/// </summary>
	public new V View
	{
		get { return (V) (object) base.View; }
	}

	/// <summary>
	/// Controller reference using the new type.
	/// </summary>
	public new C Controller
	{
		get { return (C) (object) base.Controller; }
	}
}

/// <summary>
/// Root class for the scene's scripts.
/// </summary>
public class BaseApplication : Element
{
	private void Awake()
	{
		// initialize MVC 
		_model = FindInstance(_model);
		_view = FindInstance(_view);
		_controller = FindInstance(_controller);
	}

	public Model Model
	{
		get { return _model; }
	}

	private Model _model;

	public View View
	{
		get { return _view; }
	}

	private View _view;

	public Controller Controller
	{
		get { return _controller; }
	}

	private Controller _controller;
}