﻿using UnityEngine;
using UnityEngine.UI;

public class GameView : View<AppGame>
{
	[SerializeField]
	private Button _exitBtn;

	private void Start()
	{
		// subscribe controller methods to UI events
		_exitBtn.onClick.AddListener(App.Controller.LoadHomeScene);
	}
}
