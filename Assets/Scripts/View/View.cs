﻿    /// <summary>
    /// Base class for all View related classes.
    /// </summary>
    public class View : Element
    {
	    
    }


/// <summary>
/// Base class for all View related classes.
/// </summary>
public class View<T> : View where T : BaseApplication
{
	/// <summary>
	/// Returns App as a custom 'T' type.
	/// </summary>
	public new T App
	{
		get { return (T) base.App; }
	}
}
