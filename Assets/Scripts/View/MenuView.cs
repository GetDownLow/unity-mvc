﻿using UnityEngine;
using UnityEngine.UI;

public class MenuView : View<AppMenu>
{
	[SerializeField]
	private Slider _slider;

	[SerializeField]
	private Text _text;

	[SerializeField]
	private Button _defaultValueBtn;

	[SerializeField]
	private Button _customValueBtn;

	/// <summary>
	/// Update Text
	/// </summary>
	private void SetTextValue(string newValue)
	{
		_text.text = newValue;
	}

	private void Start()
	{
		// subscribe to model for update text field
		// mvc view can access to model (mvp view can't do this)
		App.Model.ChangeValue += SetTextValue;
		// subscribe controller methods to UI events
		_defaultValueBtn.onClick.AddListener(() => App.Controller.CustomValueLoad(true));
		_customValueBtn.onClick.AddListener(() => App.Controller.CustomValueLoad(false));
		_slider.onValueChanged.AddListener(App.Controller.OnCountChange);
	}
}
