﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuModel : Model<AppMenu>
{
	public event Action<string> ChangeValue;

	[SerializeField]
	private int _defaultValue;

	private const string GameSceneName = "Game";

	private const string SpawnNRockets = "Spawn {0} rockets";

	/// <summary>
	/// Load game scene
	/// </summary>
	/// <param name="isDefaultValue"></param>
	public void LoadGameScene(bool isDefaultValue)
	{
		if (isDefaultValue)
		{
			Storage.RocketCount = _defaultValue;
		}
		SceneManager.LoadSceneAsync(GameSceneName);
	}

	public void SetRocketCount(int value)
	{
		// set static variable for all scenes
		Storage.RocketCount = value;
		if (ChangeValue != null)
		{
			//string logic can be moved in controller
			ChangeValue(StringController.Format(SpawnNRockets, Storage.RocketCount));
		}
	}
}
