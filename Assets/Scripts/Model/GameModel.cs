﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameModel : Model<AppGame>
{
	private const string MenuSceneName = "Menu";

	[SerializeField]
	private GameObject _rocketGameObject;

	public void LoadHomeScene()
	{
		SceneManager.LoadSceneAsync(MenuSceneName);
	}

	public void SpawnRocket()
	{
		// get xPos & yPos
		float screenRatio = Screen.width / (float)Screen.height;
		var startX = Camera.main.orthographicSize * -screenRatio + 0.1f;
		var startY = -Camera.main.orthographicSize + 0.1f;
		for (int i = 0; i < Storage.RocketCount; i++)
		{
			// spawn rocket. Can be pooled, but this starts only at the beginning
			var rocket = Instantiate(_rocketGameObject, new Vector2(startX + i * 0.12f, startY), 
				Quaternion.identity) as GameObject;
			// activate rocket
			var rocketComponent = rocket.GetComponent<Rocket>();
			if (rocketComponent != null)
			{
				StartCoroutine(rocketComponent.Activate(0.5f + 0.5f * i));
			}
		}
	}
}
