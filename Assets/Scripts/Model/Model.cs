﻿    /// <summary>
    /// Base class for all Model related classes.
    /// </summary>
    public class Model : Element
    {

    }


/// <summary>
/// Base class for all Model related classes.
/// </summary>
public class Model<T> : Model where T : BaseApplication
{
	/// <summary>
	/// Returns App as a custom 'T' type.
	/// </summary>
	public new T App
	{
		get { return (T) base.App; }
	}
}