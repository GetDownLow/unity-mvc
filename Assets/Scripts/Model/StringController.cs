﻿using System.Text;

public static class StringController
{
	private static StringBuilder _sb = new StringBuilder();

	public static string Format(string str, params object[] args)
	{
		_sb.Length = 0;
		_sb.AppendFormat(str, args);
		return _sb.ToString();
	}
}
