﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour
{
	private bool _activate;

	private Rigidbody2D _body;

	private ParticleSystem _particleSystem;

	public IEnumerator Activate(float deley)
	{
		yield return new WaitForSeconds(deley);
		_activate = true;
		_particleSystem.Play();
	}

	private void Awake()
	{
		_body = GetComponent<Rigidbody2D>();
		_particleSystem = GetComponentInChildren<ParticleSystem>();
	}

	private void Update()
	{
		if (_activate)
		{
			_body.velocity += Vector2.up * 0.2f * Time.deltaTime;
		}
		
	}
}
