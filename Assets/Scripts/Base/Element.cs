﻿using UnityEngine;


    /// <summary>
    /// Extension of the element class to handle different BaseApplication types.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Element<T> : Element where T : BaseApplication
	{
        public T CachedApp { get { return (T) App; } }
    }

/// <summary>
/// Base class for all MVC related classes.
/// </summary>
public class Element : MonoBehaviour
{
	/// <summary>
	/// Reference to the root application of the scene.
	/// </summary>
	public BaseApplication App
	{
		get { return _app; }
	}

	private BaseApplication _app;

	/// <summary>
	/// Finds a instance of 'T'
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="var"></param>
	/// <param name="isGlobal"></param>
	/// <returns></returns>
	public T FindInstance<T>(T var, bool isGlobal = false) where T : Object
	{
		return var ?? isGlobal ? FindObjectOfType<T>() : transform.GetComponentInChildren<T>();
	}

	private void Awake()
	{
		_app = FindInstance(_app, true);
	}
}