﻿using System;

public class MenuController : Controller<AppMenu>
{
	/// <summary>
	/// Set rocket count when UI is changed
	/// </summary>
	/// <param name="count"></param>
	public void OnCountChange(float count = 1)
	{
		App.Model.SetRocketCount((int)count);
	}

	public void CustomValueLoad(bool isDefault)
	{
		App.Model.LoadGameScene(isDefault);
	}

	/// <summary>
	/// Start setup
	/// </summary>
	private void Start()
	{
		OnCountChange();
	}


}
