﻿/// <summary>
/// Base class for all Controllers in the application.
/// </summary>
public class Controller : Element
{

}

/// <summary>
/// Base class for all Controller related classes.
/// </summary>
public class Controller<T> : Controller where T : BaseApplication
{
	/// <summary>
	/// Returns App as a custom 'T' type.
	/// </summary>
	public new T App
	{
		get { return (T) base.App; }
	}
}