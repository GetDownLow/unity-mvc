﻿using System;

public class GameController : Controller<AppGame>
{

	public void LoadHomeScene()
	{
		App.Model.LoadHomeScene();
	}

	private void Start()
	{
		App.Model.SpawnRocket();
	}
}
